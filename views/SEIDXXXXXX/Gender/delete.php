<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Gender\Gender;
use App\Utility\Utility;
$obj = new Gender();
$obj->setData($_GET);
$oneData  =  $obj->delete();
$path = $_SERVER['HTTP_REFERER'];
Utility::redirect($path);

?>