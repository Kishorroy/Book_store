<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;
use App\BookTitle\BookTitle;


$obj = new BookTitle();
$obj->setData($_GET);

$oneData  =  $obj->view();


?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>
        Atomic project
    </title>

    <link rel="stylesheet" href="../style.css">

    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.css">
    <script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
    <script src="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

    <!-- required for search, block3 of 5 end -->



</head>
<body background="../../../resources/images/atom9.png">

<div id="MessageShowDiv" style="height: 20px">
    <div id="message" class="btn-danger text-center">
        <?php
        if(isset($_SESSION['message'])){
            echo Message::message();
        }
        ?>
    </div>
</div>

<div style="text-align: center;font-size: xx-large;font-family: 'Lucida Calligraphy';color:#2098d1;background: rgba(0,0,0,0.5);padding-top: 30px;">
    <b>ATOMIC PROJECT</b>
    <br>

</div>


<nav class="navbar" style="font-family: 'Comic Sans MS'; background: rgba(0,0,0,0.5)" >
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a class="hvr-underline-from-center" href="../index.html">Home</a></li>
                <li><a class="hvr-underline-from-center" href="../BookTitle/index.php">Book Title</a></li>
                <li><a class="hvr-underline-from-center" href="../Birthday/index.php">Birthday</a></li>
                <li><a class="hvr-underline-from-center" href="../City/index.php">City</a></li>
                <li><a class="hvr-underline-from-center" href="../Email/index.php">Email</a></li>
                <li><a class="hvr-underline-from-center" href="../Gender/index.php">Gender</a></li>
                <li><a class="hvr-underline-from-center" href="../Hobbies/index.php">Hobbies</a></li>
                <li><a class="hvr-underline-from-center" href="../ProfilePicture/index.php">Profile Picture</a></li>
                <li><a class="hvr-underline-from-center" href="../Organization/index.php">Summary of organization</a></li>
            </ul>



        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container">
    <div class="navbar">
        <a href='create.php' class='btn btn-lg bg-success'>Create</a>
        <a href='index.php' class='btn btn-lg bg-danger'>Active List</a>
        <a href='trashed.php' class='btn btn-lg bg-danger'>Trashed List</a>

    </div>

<?php

         echo "
             <div class=\"bg-info text-center\" style=\"font-family: 'Comic Sans MS'; background: rgba(0,0,0,0.5);color:#2098d1;\"><h1>Single Book Information</h1></div>

               
             <table class='table table-bordered table-striped' style=\"text-align:center;font-family: 'Comic Sans MS'; background: rgba(0,0,0,0.5);color:#2098d1\">

             
                    <tr style='background-color:rgba(0,0,0,0.5)'>                   
                        <td>  <b>ID</b>  </td>                
                        <td>  <b>$oneData->id</b>  </td>                
                      
                    </tr>
        
                     <tr style='background-color:rgba(0,0,0,0.5)'>                   
                        <td>  <b>Book Title</b>  </td>                
                        <td>  <b>$oneData->book_title</b>  </td>                
                      
                    </tr>
                         
                     <tr style='background-color:rgba(0,0,0,0.5)'>                   
                        <td>  <b>Author Name</b>  </td>                
                        <td>  <b>$oneData->author_name</b>  </td>                
                      
                    </tr>
                
             
             </table>

         ";


?>

</div>

</body>
</html>